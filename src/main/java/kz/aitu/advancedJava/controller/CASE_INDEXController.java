package kz.aitu.advancedJava.controller;


import kz.aitu.advancedJava.repository.CaseindexRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class CASE_INDEXController {
    private final CaseindexRepository caseindexRepository;

    public CASE_INDEXController(CaseindexRepository caseindexRepository) {
        this.caseindexRepository = caseindexRepository;
    }


    @GetMapping("/api/CASE_INDEX")
    public ResponseEntity<?> getCASE_INDEX() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(caseindexRepository.findAll());
    }
}
