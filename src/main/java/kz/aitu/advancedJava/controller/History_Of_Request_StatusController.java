package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.HistoryofrequeststatusRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class History_Of_Request_StatusController {
    private final HistoryofrequeststatusRepository historyofrequeststatusRepository;

    public History_Of_Request_StatusController(HistoryofrequeststatusRepository historyofrequeststatusRepository) {
        this.historyofrequeststatusRepository = historyofrequeststatusRepository;
    }


    @GetMapping("/api/History_Of_Request_Status")
    public ResponseEntity<?> getHistory_Of_Request_Status() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(historyofrequeststatusRepository.findAll());
    }
}
