package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.CatalogcaseRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class Catalog_CaseController {
    private final CatalogcaseRepository catalogcaseRepository;

    public Catalog_CaseController(CatalogcaseRepository catalogcaseRepository) {
        this.catalogcaseRepository = catalogcaseRepository;
    }


    @GetMapping("/api/CATALOG_CASE")
    public ResponseEntity<?> getCATALOG_CASE() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(catalogcaseRepository.findAll());
    }
}
