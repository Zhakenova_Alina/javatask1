package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.CaseRepository;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class CaseController {
    private final CaseRepository caseRepository;

    public CaseController(CaseRepository caseRepository) {
        this.caseRepository = caseRepository;
    }


    @GetMapping("/api/CASE")
    public ResponseEntity<?> getCASE() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(caseRepository.findAll());
    }
}
