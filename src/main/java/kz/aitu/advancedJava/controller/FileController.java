package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.FileRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class FileController  {
    private final FileRepository fileRepository;

    public FileController(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }


    @GetMapping("/api/FILE")
    public ResponseEntity<?> getFILE() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(fileRepository.findAll());
    }
}
