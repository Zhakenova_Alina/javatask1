package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.ShareRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class ShareController {
    private final ShareRepository shareRepository;

    public ShareController(ShareRepository shareRepository) {
        this.shareRepository = shareRepository;
    }


    @GetMapping("/api/SHARE")
    public ResponseEntity<?> getSHARE() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(shareRepository.findAll());
    }
}
