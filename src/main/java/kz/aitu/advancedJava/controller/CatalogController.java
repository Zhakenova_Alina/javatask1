package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.CatalogRepository;
;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class CatalogController {
    private final CatalogRepository catalogRepository;

    public CatalogController(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }


    @GetMapping("/api/CATALOG")
    public ResponseEntity<?> getCATALOG() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(catalogRepository.findAll());
    }
}
