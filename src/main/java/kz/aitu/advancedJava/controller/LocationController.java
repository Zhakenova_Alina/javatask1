package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.LocationRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class LocationController {
    private final LocationRepository locationRepository;

    public LocationController(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }


    @GetMapping("/api/LOCATION")
    public ResponseEntity<?> getLOCATION() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(locationRepository.findAll());
    }
}
