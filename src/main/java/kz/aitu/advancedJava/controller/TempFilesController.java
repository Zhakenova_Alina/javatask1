package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.TempfilesRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class TempFilesController {
    private final TempfilesRepository tempfilesRepository;

    public TempFilesController(TempfilesRepository tempfilesRepository) {
        this.tempfilesRepository = tempfilesRepository;
    }


    @GetMapping("/api/TempFiles")
    public ResponseEntity<?> getTempFiles() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(tempfilesRepository.findAll());
    }
}
