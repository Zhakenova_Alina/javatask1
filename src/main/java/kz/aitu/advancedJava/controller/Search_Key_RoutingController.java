package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.SearchkeyroutingRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class Search_Key_RoutingController {
    private final SearchkeyroutingRepository searchkeyroutingRepository;

    public Search_Key_RoutingController(SearchkeyroutingRepository searchkeyroutingRepository) {
        this.searchkeyroutingRepository = searchkeyroutingRepository;
    }


    @GetMapping("/api/SEARCH_KEY_ROUTING")
    public ResponseEntity<?> getSEARCH_KEY_ROUTING() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(searchkeyroutingRepository.findAll());
    }
}
