package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.ActivityjournalRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class ActivityJournalController {
    private final ActivityjournalRepository activityjournalRepository;

    public ActivityJournalController(ActivityjournalRepository activityjournalRepository) {
        this.activityjournalRepository = activityjournalRepository;
    }


    @GetMapping("/api/ACTIVITY_JOURNAL")
    public ResponseEntity<?> getActivity_Journal() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(activityjournalRepository.findAll());
    }


}