package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.AuthorizationRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.Date;

public class AuthorizationController {
    private final AuthorizationRepository authorizationRepository;

    public AuthorizationController(AuthorizationRepository authorizationRepository) {
        this.authorizationRepository = authorizationRepository;
    }


    @GetMapping("/api/AUTHORIZATION")
    public ResponseEntity<?> getAUTHORIZATION() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(authorizationRepository.findAll());
    }


}
