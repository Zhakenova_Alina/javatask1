package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.CompanyRepository;
import kz.aitu.advancedJava.repository.CompanyunitRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class CompanyController  {
    private final CompanyRepository companyRepository;

    public CompanyController(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }


    @GetMapping("/api/Company")
    public ResponseEntity<?> getCompany() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(companyRepository.findAll());
    }
}
