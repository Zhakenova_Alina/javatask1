package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.RecordRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class RecordController {
    private final RecordRepository recordRepository;

    public RecordController(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }


    @GetMapping("/api/RECORD")
    public ResponseEntity<?> getRECORD() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(recordRepository.findAll());
    }
}
