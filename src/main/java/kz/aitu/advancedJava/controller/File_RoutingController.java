package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.FileroutingRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class File_RoutingController {
    private final FileroutingRepository fileroutingRepository;

    public File_RoutingController(FileroutingRepository fileroutingRepository) {
        this.fileroutingRepository = fileroutingRepository;
    }


    @GetMapping("/api/FILE_ROUTING")
    public ResponseEntity<?> getFILE_ROUTING() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(fileroutingRepository.findAll());
    }
}
