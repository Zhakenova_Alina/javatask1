package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.RequestRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class RequestController {
    private final RequestRepository requestRepository;

    public RequestController(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }


    @GetMapping("/api/REQUEST")
    public ResponseEntity<?> getREQUEST() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(requestRepository.findAll());
    }
}
