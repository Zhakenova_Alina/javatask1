package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.UsersRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class UsersController {
    private final UsersRepository usersRepository;

    public UsersController(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }


    @GetMapping("/api/USERS")
    public ResponseEntity<?> getUSERS() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(usersRepository.findAll());
    }
}
