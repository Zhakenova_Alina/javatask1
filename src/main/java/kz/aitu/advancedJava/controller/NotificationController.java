package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.NotificationRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class NotificationController {
    private final NotificationRepository notificationRepository;

    public NotificationController(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }


    @GetMapping("/api/NOTIFICATION")
    public ResponseEntity<?> getNOTIFICATION() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(notificationRepository.findAll());
    }
}
