package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.NomenclatureRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class NomenclatureController {
    private final NomenclatureRepository nomenclatureRepository;

    public NomenclatureController(NomenclatureRepository nomenclatureRepository) {
        this.nomenclatureRepository = nomenclatureRepository;
    }


    @GetMapping("/api/NOMENCLATURE")
    public ResponseEntity<?> getNOMENCLATURE() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(nomenclatureRepository.findAll());
    }
}
