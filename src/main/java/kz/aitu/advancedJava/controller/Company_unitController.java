package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.CompanyunitRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class Company_unitController {
    private final CompanyunitRepository companyunitRepository ;

    public Company_unitController(CompanyunitRepository companyunitRepository) {
        this.companyunitRepository = companyunitRepository;
    }


    @GetMapping("/api/Company_unit")
    public ResponseEntity<?> getCompany_unit() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(companyunitRepository.findAll());
    }
}

