package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.NomenclaturesummaryRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class Nomenclature_SummaryController {
    private final NomenclaturesummaryRepository nomenclaturesummaryRepository;

    public Nomenclature_SummaryController(NomenclaturesummaryRepository nomenclaturesummaryRepository) {
        this.nomenclaturesummaryRepository = nomenclaturesummaryRepository;
    }


    @GetMapping("/api/Nomenclature_Summary")
    public ResponseEntity<?> getNomenclature_Summary() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(nomenclaturesummaryRepository.findAll());
    }
}
