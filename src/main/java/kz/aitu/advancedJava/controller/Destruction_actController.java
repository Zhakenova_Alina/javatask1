package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.CompanyRepository;
import kz.aitu.advancedJava.repository.DestructionactRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class Destruction_actController {
    private final DestructionactRepository destructionactRepository;

    public Destruction_actController(DestructionactRepository destructionactRepository) {
        this.destructionactRepository = destructionactRepository;
    }


    @GetMapping("/api/DESTRUCTION_ACT")
    public ResponseEntity<?> getDESTRUCTION_ACT() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(destructionactRepository.findAll());
    }
}
