package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.repository.SearchkeyRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

public class SearchkeyController {
    private final SearchkeyRepository searchkeyRepository;

    public SearchkeyController(SearchkeyRepository searchkeyRepository) {
        this.searchkeyRepository = searchkeyRepository;
    }


    @GetMapping("/api/SEARCHKEY")
    public ResponseEntity<?> getSEARCHKEY() {

        Date date = new Date(1600317665L);
        long timestamp = date.getTime() / 1000;

        return ResponseEntity.ok(searchkeyRepository.findAll());
    }
}
