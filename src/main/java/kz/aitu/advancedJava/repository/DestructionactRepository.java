package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.DESTRUCTION_ACT;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DestructionactRepository  extends JpaRepository<DESTRUCTION_ACT, Long> {
}
