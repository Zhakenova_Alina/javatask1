package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Catalog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public@Repository
interface CatalogRepository  extends JpaRepository<Catalog, Long> {
}
