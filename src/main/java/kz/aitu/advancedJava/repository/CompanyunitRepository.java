package kz.aitu.advancedJava.repository;


import kz.aitu.advancedJava.model.Company_unit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyunitRepository extends JpaRepository<Company_unit, Long> {
}
