package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Location;
import kz.aitu.advancedJava.model.Request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepository extends JpaRepository<Request, Long> {
}
