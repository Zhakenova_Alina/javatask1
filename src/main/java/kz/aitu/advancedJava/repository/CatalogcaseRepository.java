package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Catalog_Case;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatalogcaseRepository extends JpaRepository<Catalog_Case, Long> {
}
