package kz.aitu.advancedJava.repository;


import kz.aitu.advancedJava.model.RECORD;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecordRepository extends JpaRepository<RECORD, Long> {
}
