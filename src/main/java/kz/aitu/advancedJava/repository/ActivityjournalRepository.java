package kz.aitu.advancedJava.repository;


import kz.aitu.advancedJava.model.Activityjournal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityjournalRepository extends JpaRepository<Activityjournal, Long> {
}
