package kz.aitu.advancedJava.repository;


import kz.aitu.advancedJava.model.NOMENCLATURE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NomenclatureRepository extends JpaRepository<NOMENCLATURE, Long> {
}
