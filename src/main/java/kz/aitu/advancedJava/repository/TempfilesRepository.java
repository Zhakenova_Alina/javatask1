package kz.aitu.advancedJava.repository;


import kz.aitu.advancedJava.model.TempFiles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TempfilesRepository extends JpaRepository<TempFiles, Long> {
}
