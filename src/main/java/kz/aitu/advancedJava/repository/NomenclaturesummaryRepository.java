package kz.aitu.advancedJava.repository;


import kz.aitu.advancedJava.model.NOMENCLATURE_SUMMARY;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NomenclaturesummaryRepository extends JpaRepository<NOMENCLATURE_SUMMARY, Long> {
}
