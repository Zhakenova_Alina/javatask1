package kz.aitu.advancedJava.repository;


import kz.aitu.advancedJava.model.SEARCHKEY;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchkeyRepository extends JpaRepository<SEARCHKEY, Long> {
}
