package kz.aitu.advancedJava.repository;


import kz.aitu.advancedJava.model.Fond;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FondRepository extends JpaRepository<Fond, Long> {



}