package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.SEARCH_KEY_ROUTING;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchkeyroutingRepository extends JpaRepository<SEARCH_KEY_ROUTING, Long> {
}
