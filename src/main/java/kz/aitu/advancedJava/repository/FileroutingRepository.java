package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.File_Routing;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileroutingRepository extends JpaRepository<File_Routing, Long> {
}
