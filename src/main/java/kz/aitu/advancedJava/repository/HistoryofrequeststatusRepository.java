package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.History_Of_Request_Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface HistoryofrequeststatusRepository extends JpaRepository<History_Of_Request_Status, Long> {
}
