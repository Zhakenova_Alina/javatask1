package kz.aitu.advancedJava.repository;


import kz.aitu.advancedJava.controller.CASE_INDEXController;
import kz.aitu.advancedJava.model.CASE_INDEX;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseindexRepository extends JpaRepository<CASE_INDEX, Long> {
}
