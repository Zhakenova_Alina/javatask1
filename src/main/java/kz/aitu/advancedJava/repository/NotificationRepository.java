package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.NOTIFICATION;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends JpaRepository<NOTIFICATION, Long> {
}
