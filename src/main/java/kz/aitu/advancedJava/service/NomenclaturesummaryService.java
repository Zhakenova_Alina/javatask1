package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.NOMENCLATURE_SUMMARY;
import kz.aitu.advancedJava.repository.NomenclaturesummaryRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
    public class NomenclaturesummaryService {

        public final NomenclaturesummaryRepository nomenclaturesummaryRepository;

    public NomenclaturesummaryService(NomenclaturesummaryRepository nomenclaturesummaryRepository) {
        this.nomenclaturesummaryRepository = nomenclaturesummaryRepository;
    }

    public List<NOMENCLATURE_SUMMARY> getAll() {
            return nomenclaturesummaryRepository.findAll();
        }

        public NOMENCLATURE_SUMMARY getById(Long id) {
            return nomenclaturesummaryRepository.findById(id).orElse(null);
        }

        public NOMENCLATURE_SUMMARY create(NOMENCLATURE_SUMMARY student) {
            return nomenclaturesummaryRepository.save(student);
        }

        public NOMENCLATURE_SUMMARY update(NOMENCLATURE_SUMMARY student) {
            return nomenclaturesummaryRepository.save(student);
        }

        public void delete(Long id) {
            nomenclaturesummaryRepository.deleteById(id);
        }


    }
