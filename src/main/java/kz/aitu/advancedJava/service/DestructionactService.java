package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.DESTRUCTION_ACT;
import kz.aitu.advancedJava.repository.DestructionactRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class DestructionactService {


        public final DestructionactRepository destructionactRepository;

    public DestructionactService(DestructionactRepository destructionactRepository) {
        this.destructionactRepository = destructionactRepository;
    }

    public List<DESTRUCTION_ACT> getAll() {
            return  destructionactRepository.findAll();
        }

        public DESTRUCTION_ACT getById(Long id) {
            return destructionactRepository.findById(id).orElse(null);
        }

        public DESTRUCTION_ACT create(DESTRUCTION_ACT student) {
            return destructionactRepository.save(student);
        }

        public DESTRUCTION_ACT update(DESTRUCTION_ACT student) {
            return destructionactRepository.save(student);
        }

        public void delete(Long id) {
            destructionactRepository.deleteById(id);
        }


    }
