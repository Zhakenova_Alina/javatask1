package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.Case;
import kz.aitu.advancedJava.repository.CaseRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
    public class CaseService {

        public final  CaseRepository caseRepository;

        public CaseService(CaseRepository caseRepository) {
            this.caseRepository = caseRepository;
        }

        public List<Case> getAll() {
            return caseRepository.findAll();
        }

        public Case getById(Long id) {
            return caseRepository.findById(id).orElse(null);
        }

        public Case create(Case student) {
            return caseRepository.save(student);
        }

        public Case update(Case student) {
            return caseRepository.save(student);
        }

        public void delete(Long id) {
            caseRepository.deleteById(id);
        }


    }
