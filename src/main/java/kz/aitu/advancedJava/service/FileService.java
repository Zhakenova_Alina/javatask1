package kz.aitu.advancedJava.service;


import kz.aitu.advancedJava.model.File;
import kz.aitu.advancedJava.repository.FileRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
    public class FileService {

        public final FileRepository fileRepository;

    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public List<File> getAll() {
            return fileRepository.findAll();
        }

        public File getById(Long id) {
            return fileRepository.findById(id).orElse(null);
        }

        public File create(File student) {
            return fileRepository.save(student);
        }

        public File update(File student) {
            return fileRepository.save(student);
        }

        public void delete(Long id) {
           fileRepository.deleteById(id);
        }


    }
