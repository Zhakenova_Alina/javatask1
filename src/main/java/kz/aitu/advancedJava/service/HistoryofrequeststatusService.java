package kz.aitu.advancedJava.service;


import kz.aitu.advancedJava.model.History_Of_Request_Status;
import kz.aitu.advancedJava.repository.HistoryofrequeststatusRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
    public class HistoryofrequeststatusService {

        public final HistoryofrequeststatusRepository historyofrequeststatusRepository;

    public HistoryofrequeststatusService(HistoryofrequeststatusRepository historyofrequeststatusRepository) {
        this.historyofrequeststatusRepository = historyofrequeststatusRepository;
    }

    public List<History_Of_Request_Status> getAll() {
            return historyofrequeststatusRepository.findAll();
        }

        public History_Of_Request_Status getById(Long id) {
            return historyofrequeststatusRepository.findById(id).orElse(null);
        }

        public History_Of_Request_Status create(History_Of_Request_Status student) {
            return historyofrequeststatusRepository.save(student);
        }

        public History_Of_Request_Status update(History_Of_Request_Status student) {
            return historyofrequeststatusRepository.save(student);
        }

        public void delete(Long id) {
            historyofrequeststatusRepository.deleteById(id);
        }


    }
