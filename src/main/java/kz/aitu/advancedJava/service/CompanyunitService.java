package kz.aitu.advancedJava.service;


import kz.aitu.advancedJava.model.Company_unit;
import kz.aitu.advancedJava.repository.CompanyunitRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
    public class CompanyunitService {

        public final CompanyunitRepository companyunitRepository;

    public CompanyunitService(CompanyunitRepository companyunitRepository) {
        this.companyunitRepository = companyunitRepository;
    }

    public List<Company_unit> getAll() {
            return companyunitRepository.findAll();
        }

        public Company_unit getById(Long id) {
            return companyunitRepository.findById(id).orElse(null);
        }

        public Company_unit create(Company_unit student) {
            return companyunitRepository.save(student);
        }

        public Company_unit update(Company_unit student) {
            return companyunitRepository.save(student);
        }

        public void delete(Long id) {
            companyunitRepository.deleteById(id);
        }


    }
