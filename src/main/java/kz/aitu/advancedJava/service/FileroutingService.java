package kz.aitu.advancedJava.service;


import kz.aitu.advancedJava.model.File_Routing;
import kz.aitu.advancedJava.repository.FileroutingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
    public class  FileroutingService  {

        public final FileroutingRepository fileroutingRepository;

    public FileroutingService(FileroutingRepository fileroutingRepository) {
        this.fileroutingRepository = fileroutingRepository;
    }

    public List<File_Routing> getAll() {
            return fileroutingRepository.findAll();
        }

        public File_Routing getById(Long id) {
            return fileroutingRepository.findById(id).orElse(null);
        }

        public File_Routing create(File_Routing student) {
            return fileroutingRepository.save(student);
        }

        public File_Routing update(File_Routing student) {
            return fileroutingRepository.save(student);
        }

        public void delete(Long id) {
            fileroutingRepository.deleteById(id);
        }


    }
