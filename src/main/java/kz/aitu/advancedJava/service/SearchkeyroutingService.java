package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.SEARCH_KEY_ROUTING;
import kz.aitu.advancedJava.repository.SearchkeyroutingRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
    public class SearchkeyroutingService {

        public final SearchkeyroutingRepository searchkeyroutingRepository;

        public SearchkeyroutingService(SearchkeyroutingRepository searchkeyroutingRepository) {
            this.searchkeyroutingRepository = searchkeyroutingRepository;
        }

        public List<SEARCH_KEY_ROUTING> getAll() {
            return searchkeyroutingRepository.findAll();
        }

        public SEARCH_KEY_ROUTING getById(Long id) {
            return searchkeyroutingRepository.findById(id).orElse(null);
        }

        public SEARCH_KEY_ROUTING create(SEARCH_KEY_ROUTING student) {
            return searchkeyroutingRepository.save(student);
        }

        public SEARCH_KEY_ROUTING update(SEARCH_KEY_ROUTING student) {
            return searchkeyroutingRepository.save(student);
        }

        public void delete(Long id) {
            searchkeyroutingRepository.deleteById(id);
        }


    }
