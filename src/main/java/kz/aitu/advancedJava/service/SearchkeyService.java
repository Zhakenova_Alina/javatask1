package kz.aitu.advancedJava.service;


import kz.aitu.advancedJava.model.SEARCHKEY;
import kz.aitu.advancedJava.repository.SearchkeyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
    public class SearchkeyService {

        public final SearchkeyRepository searchkeyRepository;

    public SearchkeyService(SearchkeyRepository searchkeyRepository) {
        this.searchkeyRepository = searchkeyRepository;
    }

    public List<SEARCHKEY> getAll() {
            return searchkeyRepository.findAll();
        }

        public SEARCHKEY getById(Long id) {
            return searchkeyRepository.findById(id).orElse(null);
        }

        public SEARCHKEY create(SEARCHKEY student) {
            return searchkeyRepository.save(student);
        }

        public SEARCHKEY update(SEARCHKEY student) {
            return searchkeyRepository.save(student);
        }

        public void delete(Long id) {
            searchkeyRepository.deleteById(id);
        }


    }
