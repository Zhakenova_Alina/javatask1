package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.RECORD;
import kz.aitu.advancedJava.repository.RecordRepository;
import org.springframework.stereotype.Service;

import java.util.List;



    @Service
    public class RecordService {

        public final RecordRepository recordRepository;

        public RecordService(RecordRepository recordRepository) {
            this.recordRepository = recordRepository;
        }

        public List<RECORD> getAll() {
            return recordRepository.findAll();
        }

        public RECORD getById(Long id) {
            return recordRepository.findById(id).orElse(null);
        }

        public RECORD create(RECORD student) {
            return recordRepository.save(student);
        }

        public  RECORD update(RECORD student) {
            return recordRepository.save(student);
        }

        public void delete(Long id) {
            recordRepository.deleteById(id);
        }


    }
