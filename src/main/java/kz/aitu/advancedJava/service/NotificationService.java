package kz.aitu.advancedJava.service;


import kz.aitu.advancedJava.model.NOTIFICATION;
import kz.aitu.advancedJava.repository.NotificationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
    public class NotificationService {

        public final NotificationRepository notificationRepository;

    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public List<NOTIFICATION> getAll() {
            return notificationRepository.findAll();
        }

        public NOTIFICATION getById(Long id) {
            return notificationRepository.findById(id).orElse(null);
        }

        public NOTIFICATION create(NOTIFICATION student) {
            return notificationRepository.save(student);
        }

        public NOTIFICATION update(NOTIFICATION student) {
            return notificationRepository.save(student);
        }

        public void delete(Long id) {
           notificationRepository.deleteById(id);
        }


    }
