package kz.aitu.advancedJava.service;


import kz.aitu.advancedJava.model.Activityjournal;
import kz.aitu.advancedJava.repository.ActivityjournalRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityjournalService {

    public final ActivityjournalRepository activityjournalRepository;

    public ActivityjournalService(ActivityjournalRepository activityjournalRepository) {
        this.activityjournalRepository = activityjournalRepository;
    }


    public List<Activityjournal> getAll() {
        return activityjournalRepository.findAll();
    }

    public Activityjournal getById(Long id) {
        return activityjournalRepository.findById(id).orElse(null);
    }

    public Activityjournal create(Activityjournal student) {
        return activityjournalRepository.save(student);
    }

    public Activityjournal update(Activityjournal student) {
        return activityjournalRepository.save(student);
    }

    public void delete(Long id) {
        activityjournalRepository.deleteById(id);
    }
}



