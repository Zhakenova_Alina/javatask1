package kz.aitu.advancedJava.service;


import kz.aitu.advancedJava.model.CASE_INDEX;
import kz.aitu.advancedJava.repository.CaseindexRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
    public class CaseindexService{

        public final CaseindexRepository caseindexRepository;

    public CaseindexService(CaseindexRepository caseindexRepository) {
        this.caseindexRepository = caseindexRepository;
    }

    public List<CASE_INDEX> getAll() {
            return caseindexRepository.findAll();
        }

        public CASE_INDEX getById(Long id) {
            return caseindexRepository.findById(id).orElse(null);
        }

        public CASE_INDEX create(CASE_INDEX student) {
            return caseindexRepository.save(student);
        }

        public CASE_INDEX update(CASE_INDEX student) {
            return caseindexRepository.save(student);
        }

        public void delete(Long id) {
            caseindexRepository.deleteById(id);
        }



    }

