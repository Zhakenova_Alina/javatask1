package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.NOMENCLATURE;
import kz.aitu.advancedJava.repository.NomenclatureRepository;
import org.springframework.stereotype.Service;

import java.util.List;



    @Service
    public class NomenclatureService {

        public final NomenclatureRepository nomenclatureRepository;

        public NomenclatureService(NomenclatureRepository nomenclatureRepository) {
            this.nomenclatureRepository = nomenclatureRepository;
        }

        public List<NOMENCLATURE> getAll() {
            return nomenclatureRepository.findAll();
        }

        public NOMENCLATURE getById(Long id) {
            return nomenclatureRepository.findById(id).orElse(null);
        }

        public NOMENCLATURE create(NOMENCLATURE student) {
            return nomenclatureRepository.save(student);
        }

        public NOMENCLATURE update(NOMENCLATURE student) {
            return nomenclatureRepository.save(student);
        }

        public void delete(Long id) {
           nomenclatureRepository.deleteById(id);
        }


    }
