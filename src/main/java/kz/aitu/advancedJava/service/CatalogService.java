package kz.aitu.advancedJava.service;


import kz.aitu.advancedJava.model.Catalog;
import kz.aitu.advancedJava.repository.CatalogRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogService {

    public final CatalogRepository catalogRepository;

    public CatalogService(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    public List<Catalog> getAll() {
        return catalogRepository.findAll();
    }

    public Catalog getById(Long id) {
        return catalogRepository.findById(id).orElse(null);
    }

    public Catalog create(Catalog student) {
        return catalogRepository.save(student);
    }

    public Catalog update(Catalog student) {
        return catalogRepository.save(student);
    }

    public void delete(Long id) {
        catalogRepository.deleteById(id);
    }
}


