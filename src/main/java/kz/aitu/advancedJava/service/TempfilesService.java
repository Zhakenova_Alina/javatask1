package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.TempFiles;
import kz.aitu.advancedJava.repository.TempfilesRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
    public class TempfilesService {

        public final TempfilesRepository tempfilesRepository;

        public TempfilesService(TempfilesRepository tempfilesRepository) {
            this.tempfilesRepository = tempfilesRepository;
        }

        public List<TempFiles> getAll() {
            return tempfilesRepository.findAll();
        }

        public TempFiles getById(Long id) {
            return tempfilesRepository.findById(id).orElse(null);
        }

        public TempFiles create(TempFiles student) {
            return tempfilesRepository.save(student);
        }

        public TempFiles update(TempFiles student) {
            return tempfilesRepository.save(student);
        }

        public void delete(Long id) {
           tempfilesRepository.deleteById(id);
        }


    }
