package kz.aitu.advancedJava.service;


import kz.aitu.advancedJava.model.Catalog_Case;
import kz.aitu.advancedJava.repository.CatalogcaseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogcaseService {

    public final CatalogcaseRepository catalogcaseRepository;

    public CatalogcaseService(CatalogcaseRepository catalogcaseRepository) {
        this.catalogcaseRepository = catalogcaseRepository;
    }

    public List<Catalog_Case> getAll() {
        return catalogcaseRepository.findAll();
    }

    public Catalog_Case getById(Long id) {
        return catalogcaseRepository.findById(id).orElse(null);
    }

    public Catalog_Case create(Catalog_Case student) {
        return catalogcaseRepository.save(student);
    }

    public Catalog_Case update(Catalog_Case student) {
        return catalogcaseRepository.save(student);
    }

    public void delete(Long id) {
        catalogcaseRepository.deleteById(id);
    }
}


